
# A-Tune

### 工作目标和范围

- 调优领域相关技术探索
- AI辅助性能分析



# 组织会议

- 每双周周一下午2:30-4:30
- 通过邮件申报议题


# 成员


### Maintainer列表

- xiezhipeng1[@xiezhipeng1](https://gitee.com/xiezhipeng1)


### Committer列表

- licihua[@licihua](https://gitee.com/licihua)
- shanshishi[@shanshishi](https://gitee.com/shanshishi)
- hanxinke[@hanxinke](https://gitee.com/hanxinke)


# 联系方式

- 邮件列表: a-tune@openeuler.org



# 项目清单


项目名称：

- A-Tune

repository地址：

- https://gitee.com/openeuler/A-Tune
- https://gitee.com/src-openeuler/A-Tune
- https://gitee.com/openeuler/prefetch_tuning
- https://gitee.com/src-openeuler/prefetch_tuning

