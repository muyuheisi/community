﻿***注意：本文档所有的斜体内容，请在完成时删除***

# RISC-V

- 编译构建openEuler的RISC-V运行版本
- 搭建openEuler源码包的RISC-V交叉编译环境，并构建封装交叉编译环境QEMU/KVM虚拟机镜像
- 开发openEuler的RISC-V版本构建流程脚本
- SRPM适应RISC-V架构编译的必要补丁
- 先构建openEuler面向RISC-V的最小可运行版本（约120多个包），然后迭代增加支持大部分包(约2000多个包，按照社区讨论可以调整）
- 及时响应用户反馈，解决相关问题

# 组织会议

- 公开的会议时间：北京时间，每周X 下午，XX点~XX点

# 成员

### Maintainer列表

- Peng Zhou[@zhoupeng01](https://gitee.com/zhoupeng01)，zhoupeng@iscas.ac.cn
- Xuzhou Zhang[@whoisxxx](https://gitee.com/whoisxxx)，zhangxuzhou4@huawei.com


### Committer列表

- Peng Zhou[@zhoupeng01](https://gitee.com/zhoupeng01)
- Xuzhou Zhang[@whoisxxx](https://gitee.com/whoisxxx)




# 联系方式

- [邮件列表](dev@openeuler.org)
- [IRC公开会议]()
- 视频会议



# 项目清单

*<可选，如果在申请SIG的时候，就有新项目，请完善此处内容。项目名称和申请表格一致，repository地址和repository.yaml内的申请地址一致>*

项目名称：

repository地址：

- 新建(进行中)
- 
