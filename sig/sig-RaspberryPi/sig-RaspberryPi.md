# openEuler RaspberryPi Special Interest Group (SIG)
English | [简体中文](./sig-RaspberryPi_cn.md)

The openEuler RaspberryPi SIG aims at migrating openEuler to Raspberry Pi, in order to reduce the threshold for using openEuler and promote openEuler to new users.


## SIG Mission and Scope

### Mission
- Add RaspberryPi support for openEuler community
- According to the iterative version of openEuler, constantly complete the migration of openEuler to Raspberry Pi, and provide scripts for building openEuler image on Raspberry Pi and documents
- Respond to users' feedback in a timely manner and resolve the problems

### Scope

- Migrate openEuler kernel to Raspberry PI, with further maintenance and upgrades
- Create and update openEuler image running on Raspberry Pi, provide image's build scripts and documents
- Manage the documentations, meetings, mailist and IRC within RaspberryPi SIG

### Deliverables

- Source and tar

### Repositories and description managed by this SIG

- Repository of scripts for building openEuler image on Raspberry Pi and documents: https://gitee.com/openeuler/raspberrypi
- Repository of openEuler kernel source for Raspberry Pi：https://gitee.com/peneuler/raspberrypi-kernel
- SRPM Repository of openEuler kernel for Raspberry Pi: https://gitee.com/src-openeuler/raspberrypi-kernel
- SRPM Repository of firmware for Raspberry Pi: https://gitee.com/src-openeuler/raspberrypi-firmware
- SRPM Repository of scripts of building images for Raspberry Pi: https://gitee.com/src-openeuler/raspberrypi-build

## Basic Information

### Project Introduction
    https://gitee.com/openeuler/community/tree/master/sig/sig-RaspberryPi/

### Maintainers
- jianminw
- woqidaideshi

### Committers
- jianminw
- woqidaideshi

### Mailing list
- dev@openeuler.org

### Slack Workspace
- https://openeuler-raspberrypi.slack.com
  - The link to join this workspace will be periodically updated in [raspberrypi repo](https://gitee.com/openeuler/raspberrypi)

### Meeting
- Time: The first and third Tuesday of every month, 15:00 - 15:30 +0800
- Zoom MeetID: 881 4204 8958

### IRC Channel
- #openeuler-raspberrypi

### External Contact
- woqidaideshi
