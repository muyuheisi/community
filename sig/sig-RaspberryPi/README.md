# openEuler RaspberryPi SIG

- 在 openEuler 社区中添加对 Raspberry Pi 的支持
- 根据 openEuler 迭代版本，持续完成 openEuler 向树莓派的移植，并提供 openEuler 的 Raspberry Pi 版本镜像的构建脚本和使用文档
- 及时响应用户反馈，解决相关问题

# 组织会议

- 公开的会议时间：北京时间，每周二 下午，15:00 - 15:30
- Zoom MeetID: 881 4204 8958

# 成员

### Maintainer 列表

- 王建民[@jianminw](https://gitee.com/jianminw)
- 方亚芬[@woqidaideshi](https://gitee.com/woqidaideshi)

### Committer 列表

- 王建民[@jianminw](https://gitee.com/jianminw)
- 方亚芬[@woqidaideshi](https://gitee.com/woqidaideshi)

# 联系方式

- 邮件列表: dev@openeuler.org
- Slack 群组: https://openeuler-raspberrypi.slack.com ，加入 Slack 群组的链接将定期更新在 [树莓派 SIG 主仓库](https://gitee.com/openeuler/raspberrypi)
- IRC 频道: #openeuler-raspberrypi
- Zoom 公开会议 MeetID: 881 4204 8958

# 项目清单

项目名称：RaspberryPi

repository 地址：

- https://gitee.com/openeuler/raspberrypi
- https://gitee.com/openeuler/raspberrypi-kernel
- https://gitee.com/src-openeuler/raspberrypi-kernel
- https://gitee.com/src-openeuler/raspberrypi-firmware
- https://gitee.com/src-openeuler/raspberrypi-build
