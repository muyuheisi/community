# SIG-Security_facility

## SIG组工作目标和范围

sig-security_facility主要讨论在openEuler社区版本中已有或未来规划的安全技术：

- 在openEuler社区版本中使能主流的Linux安全特性，提供系统安全工具、库、基础设施等，提升系统的安全性

- 改善现有安全技术的应用体验，帮助安全创造实际的价值

- 讨论openEuler未来安全技术的规划


 ### 该SIG管理的repository及描述

- 项目名称：
  - 交付件形式：源码、tar包或兼而有之
  - openeuler/apparmor


 ### 跨领域和面向外部的流程

 由该SIG定义和执行的，且跨领域和面向外部的流程和行动：

 - 非内部流程清单
 - 该SIG拥有的面向整个openEulerSIG的组织指导计划等



# 组织会议

- 公开的会议时间：北京时间，每周X 下午，XX点~XX点

*<请在此给出SIG会议的时间>*



# 成员

*<请在此给出团队成员的列表>*

### Maintainer列表

- zhujianwei001

- mailofzxf

- steven



### Committer列表

- huangzq6

- 吃牛的蛙

- nettingsisyphus

- mkitgrt

- dawny_sun

- guoxiaoqi


# 联系方式

*<如果需要单独申请邮件列表，请在此补充邮箱名称：sig-security_facility@openeuler.org>*

- [邮件列表](sig-security_facility@openeuler.org)
- [IRC公开会议]()