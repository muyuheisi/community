
# 新建SIG申请
[English](./sig-template.md) | 简体中文

说明：本SIG的Charter内容遵循openEuler章程 [README](/zh/governance/README.md)中描述的约定，使用[SIG-governance](/zh/technical-committee/governance/SIG-governance.md)中概述的角色和组织管理。

## SIG组工作目标和范围
   申请该SIG组的目的是构建一个DPDK开源软件的开发平台，包括创建仓库，遵循maintainer检视机制等，
   该SIG业务范围主要是DPDK PMD驱动的开发维护，暂不需要其他SIG支持

### 该SIG管理的repository及描述
- 项目名称：dpdk
- 交付件形式：rpm包
- repository1名称：https://gitee.com/speech_white/dpdk.git

### 跨领域和面向外部的流程
由该SIG定义和执行的，且跨领域和面向外部的流程和行动：
- 非内部流程清单
- 该SIG拥有的面向整个openEulerSIG的组织指导计划等
